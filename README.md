## lineage_realme_trinket-userdebug 11 RQ3A.210805.001.A1 eng.irawan.20210817.175154 test-keys
- Manufacturer: realme
- Platform: trinket
- Codename: realme_trinket
- Brand: Realme
- Flavor: lineage_realme_trinket-userdebug
- Release Version: 11
- Id: RQ3A.210805.001.A1
- Incremental: eng.irawan.20210817.175154
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Realme/lineage_realme_trinket/realme_trinket:11/RQ3A.210805.001.A1/irawans08171751:userdebug/test-keys
- OTA version: 
- Branch: lineage_realme_trinket-userdebug-11-RQ3A.210805.001.A1-eng.irawan.20210817.175154-test-keys
- Repo: realme_realme_trinket_dump_30475


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
